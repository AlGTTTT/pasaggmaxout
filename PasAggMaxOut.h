#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>

///////////////////////////////////
#define COMMENT_OUT_ALL_PRINTS

#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

#define INITIAL_TEST_TRAN_DATASETS_INCLUDED 

	#define USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1

//#define EXIT_AFTER_TRAINING
///////////////////////////////////////////////
#define nLengthOneLineMax 200 //120000 //50000

#define nLarge 1000000
#define fLarge 1.0E+12 //9

#define NO_VecInit
//#define PRINT_DEBUGGING_INFO
#define eps 1.0E-9


#define fFeaMin (-5000.0)
#define fFeaMax 5000.0

#define nInputLineLengthMax 100
#define nSubstringLenMax 20
///////////////////////////////////////////////////

#define nNumVecTrainTot 4178 // for svmguide1_train_2000_2178.txt
//#define nNumVecTrainTot 3089 // for svmguide1_train.txt
//#define nNumVecTrainTot 5267 //3089
//#define nNumVecTrainTot 150 //for svmguide1_train_Pos49_Neg101.txt

	#define nNumVecTestTot 4000

#define nProdTrainTot (nDim*nNumVecTrainTot)
	#define nProdTestTot (nDim*nNumVecTestTot)

////////////////////////////////////////////////////////////////////
#define nDim 4 //8000
#define nDim_D (nDim)

///////////////////////////////////////////////////////////////////////////
#define nDim_H 1024
//#define nDim_H 512
//#define nDim_H 256
//#define nDim_H 128 //dimension of the nonlinear/transformed space
//#define nDim_H 64 //5 //dimension of the nonlinear/transformed space
//#define nDim_H 32
//#define nDim_H 16
//#define nDim_H 8
//#define nDim_H 4

#define nNumOfHyperplanes 480  
//#define nNumOfHyperplanes 240  
//#define nNumOfHyperplanes 120  
//#define nNumOfHyperplanes 80  
//#define nNumOfHyperplanes 50  
//#define nNumOfHyperplanes 30  
//#define nNumOfHyperplanes 15  // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 8  // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 4 //3 // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 2 //3 // the number of hyperplanes per one nonlinear fea
#define nK (nNumOfHyperplanes) // the number of hyperplanes

 //#define nNumOfItersOfTrainingTot 400 //?
//#define nNumOfItersOfTrainingTot 200 //?
//#define nNumOfItersOfTrainingTot 100 //?
//#define nNumOfItersOfTrainingTot 40 //?
#define nNumOfItersOfTrainingTot 20 
//#define nNumOfItersOfTrainingTot 10 //25 //15 
//#define nNumOfItersOfTrainingTot 4 //10 
//#define nNumOfItersOfTrainingTot 2 //3 //10 
//#define nNumOfItersOfTrainingTot 1 //10 //5 

//////////////////////////////////////////////////////////////////

#define fW_Init_Min (-1.0) 
#define fW_Init_Max 1.0

//#define fW_Init_Min (-0.1) //(-1.0) 
//#define fW_Init_Max 0.1 //(1.0) 

#define nDim_U (nDim_D*nDim_H*nK)

//#define fU_Init_Min (-1.0) 
//#define fU_Init_Max 1.0 

#define fU_Init_Min (-0.1) //(-1.0) 
#define fU_Init_Max 0.1 //(1.0) 

//#define INCLUDE_BIAS_POS_TO_NEG
// change 'nNumVecTrainTot' as well
//#define fBiasPosOrNeg (1.0) //1.0 -- no bias,  > 1.0 -- a positive bias; < 1.0 -- negative bias

////////////////////////////////////////

#define fAlpha 0.9 //const float fAlphaf, // < 1.0
#define fEpsilon 0.05 //?? -- linearly depends on variance

#define fCr 0.125
#define fC 0.125
///////////////////////////////////////////////////////////////////
//#define fR_Const 0.61803399
#define fC_Const ( (-1.0 + sqrt(5.0) )/2.0 ) //(1.0-R)
#define fR_Const (1.0 - fC_Const)

#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define SHFT2(a,b,c) (a)=(b);(b)=(c);
#define SHFT3(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define fEfficOfArr1stRequired 0.9

const float fPrecisionOf_Golden_Search = 0.01; // 0.005
const float eps_thesame = 1.0E-5; //-2; // precision of the position
/////////////////////////////////////

typedef struct
{
	int nNumOfPosit_Y_Totf;
	int	nNumOfNegat_Y_Totf;

	int	nNumOfCorrect_Y_Totf;
	int	nNumOfVecs_Totf;

	int	nNumOfPositCorrect_Y_Totf;
	int	nNumOfNegatCorrect_Y_Totf;

	float fPercentageOfCorrectTotf;
	float fPercentageOfCorrect_Positf;
	float fPercentageOfCorrect_Negatf;

} PAS_AGG_MAX_OUT_RESUTS;

